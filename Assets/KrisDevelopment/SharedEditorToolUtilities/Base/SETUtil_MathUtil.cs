﻿////////////////////////////////////////
//    Shared Editor Tool Utilities    //
//    by Kris Development             //
////////////////////////////////////////

//License: MIT
//GitLab: https://gitlab.com/KrisDevelopment/SETUtil

using U = UnityEngine;
using Mathf = UnityEngine.Mathf;

namespace SETUtil
{
	public static class MathUtil
	{
		public static float Map(float val, float low1, float high1, float low2, float high2)
		{
			//this remaps the value from its expected low and high to the new low2 and high2
			return low2 + (val - low1) * ((high2 - low2) / (high1 - low1));
		}

		public static float Saturate(float val)
		{
			return U.Mathf.Clamp(val, 0f, 1f);
		}

		public static float PerlinVolume(U.Vector3 point, float perlinScale, int seed)
		{
			/*
			//Legacy v1:
			float
				_passXY = U.Mathf.PerlinNoise((float) (seed + point.x) * perlinScale, (float) (seed + point.y) * perlinScale),
				_passZY = U.Mathf.PerlinNoise((float) (seed * 3 + point.z) * perlinScale, (float) (seed * 3 + point.y) * perlinScale),
				_passXZ = U.Mathf.PerlinNoise((float) (seed * 5 + point.x) * perlinScale, (float) (seed * 5 + point.z) * perlinScale);
			float _val = _passXY * _passZY * _passXZ;
			return _val;
			*/
			 
			//Legacy v2:
			/*
			float _perlinY = U.Mathf.PerlinNoise((seed + 100 + point.y) * perlinScale, (seed + 100 + new U.Vector2(point.x, point.y).magnitude) * perlinScale);
			float _perlinXZ = U.Mathf.PerlinNoise((seed + point.x) * perlinScale, (seed + point.z) * perlinScale);

			return (_perlinY + _perlinXZ)/2f;
			*/

			//Perlin Volume v3:
			U.Vector3 _noisePoint = new U.Vector3(seed + point.x * perlinScale,
				(seed + 5) + point.y * perlinScale,
				(seed - 7) + point.z * perlinScale);
			
			float _ab = Mathf.PerlinNoise(_noisePoint.x, _noisePoint.y);
			float _bc = Mathf.PerlinNoise(_noisePoint.y, _noisePoint.z);
			float _ac = Mathf.PerlinNoise(_noisePoint.x, _noisePoint.z);
			
			float _ba = Mathf.PerlinNoise(_noisePoint.y, _noisePoint.x);
			float _cb = Mathf.PerlinNoise(_noisePoint.z, _noisePoint.y);
			float _ca = Mathf.PerlinNoise(_noisePoint.z, _noisePoint.x);

			return (_ab + _bc + _ac + _ba + _cb + _ca) / 6f;
		}
	}
}
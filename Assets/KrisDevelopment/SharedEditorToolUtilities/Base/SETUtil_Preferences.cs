﻿////////////////////////////////////////
//    Shared Editor Tool Utilities    //
//    by Kris Development             //
////////////////////////////////////////

//License: MIT
//GitLab: https://gitlab.com/KrisDevelopment/SETUtil

namespace SETUtil
{
	//SETUtil Enums:
	public enum DebugPreference
	{
		Message,
		Warning,
		Error
	}

	//SETUtil Preferences:
	public static class Preferences
	{
		//SETUtil Properties:
		public static int STACK_MAX = 500; //0 = is no limit
		public static DebugPreference DEBUG_PREF = DebugPreference.Message;
	}
}
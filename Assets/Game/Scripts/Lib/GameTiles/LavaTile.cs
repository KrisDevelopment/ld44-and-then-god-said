using UnityEngine;

public class LavaTile : LiquidTile
{
	public LavaTile(int x, int y) : base(x, y, new Color(1f, 0.35f, .2f)) {}


	public override void OnTick ()
	{
		var _tileGrid = GameMaster.Instance().tileGrid;
		
		WaterInteraction(1, 0, _tileGrid);
		WaterInteraction(-1, 0, _tileGrid);
		WaterInteraction(0, gravityDirection, _tileGrid);

		base.OnTick();
	}

	private void WaterInteraction (int xDir, int yDir, TileGrid _tileGrid)
	{
		var _interactionTile = _tileGrid.GetTileAt(x + xDir, y + yDir);
		if(_interactionTile is WaterTile){
			_interactionTile.Destroy();
			new StoneTile(_interactionTile.x, _interactionTile.y);
		}
	}
}
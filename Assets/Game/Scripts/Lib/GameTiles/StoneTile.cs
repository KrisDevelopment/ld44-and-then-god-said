using UnityEngine;

public class StoneTile : BaseTile
{
	public StoneTile(int x, int y) : base(x, y)
	{
		this.visual = new TileVisual(new Color(.6f, .6f, .6f), AssetRepo.Instance().earthTileSprite);
	}
}
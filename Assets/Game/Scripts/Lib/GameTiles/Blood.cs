using UnityEngine;

public class BloodTile : LiquidTile
{
	private int life = 60;

	public BloodTile(int x, int y) : base(x, y, new Color(1, .1f, .1f)) {}

	public override void OnTick() 
	{
		base.OnTick();

		if(life <= 0){
			Destroy();
		}
		
		life--;
	}
}
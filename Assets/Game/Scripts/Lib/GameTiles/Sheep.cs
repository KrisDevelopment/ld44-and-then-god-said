using UnityEngine;

public class Sheep : CreatureTile
{
	public enum State 
	{
		Idle,
		Eating,
		//Dead,
	}

	protected int idleTicks = 8;
	protected int maxHunger = 80;

	protected int currentIdle = 0;
	protected int hunger = 0;

	public bool dead {get; private set;}

	private SheepVisual sheepVisual;

	public State state = State.Idle;

	private int horizontalInterest = 0;

	
	public Sheep(int x, int y) : base(x, y)
	{
		gravityDirection = -1;
		visual = sheepVisual = new SheepVisual();
		hunger = 0;

		GameMaster.Instance().gameStats.grace += 20;
	}

	public override void OnTick()
	{
		base.OnTick();

		if(!dead){
			hunger++;

			if(currentIdle >= idleTicks){
				if(hunger > maxHunger/3){
					// if needs food

					var _tileGrid = GameMaster.Instance().tileGrid;
					var _stepOnTile = _tileGrid.GetTileAt(x, y + gravityDirection);
					DirtTile _dirtTile = null;

					if(_stepOnTile is DirtTile){
						_dirtTile = _stepOnTile as DirtTile;

						if(_dirtTile.grassStage > 0){
							// can eat at that tile
							hunger = 0;
							_dirtTile.SetGrassStage(_dirtTile.grassStage - 1);
							sheepVisual.SetSheepState(State.Eating);
							GameMaster.Instance().gameStats.grace++;
						}
					}
				}else{
					// if roaming around
					horizontalInterest = Random.Range(-1, 2);
					sheepVisual.SetSheepState(State.Idle);
					sheepVisual.SetLookDirection(horizontalInterest);
					UpdateVisuals();
				}

				currentIdle = 0;
			}else{
				currentIdle++;
			}

			sheepVisual.SetSheepHungerVisual(1f - (float) hunger/maxHunger);

			if(hunger > maxHunger){
				dead = true;
				hunger = maxHunger;
			}
		}else{
			// if dead, then dispaly the dead body and wait a bit before disposing of it
			
			//sheepVisual.SetSheepState(State.Dead);

			if(hunger <= 0){
				Kill();
			}

			hunger--;
		}
	}

	public override void Destroy()
	{
		base.Destroy();
		GameMaster.Instance().gameStats.grace -= 20;
	}

	protected override void MovementBehavior()
	{
		VerticalGravity();
		var _tileGrid = GameMaster.Instance().tileGrid;

		if(!CanGoAtRelative(horizontalInterest, gravityDirection) && CanGoAtRelative(horizontalInterest, 0) && !(_tileGrid.GetTileAt(horizontalInterest, y + gravityDirection) is LiquidTile)){
			x += horizontalInterest;
			horizontalInterest = 0;
		}
	}
}
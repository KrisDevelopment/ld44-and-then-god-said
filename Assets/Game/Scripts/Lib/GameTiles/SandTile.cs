using UnityEngine;

public class SandTile : PhysicsTile
{
	public SandTile(int x, int y) : base (x,y)
	{
		this.gravityDirection = -1;
		this.visual = new TileVisual(new Color (.85f, .7f, .16f), AssetRepo.Instance().earthTileSprite);
	}

	protected override void MovementBehavior ()
	{
		SandMovement();
	}

	protected void SandMovement ()
	{
		if(CanGoAtRelative(0, gravityDirection)) {
			y += gravityDirection;
		}else{
			if(CanGoAtRelative(-1, 0) && CanGoAtRelative(-1 , gravityDirection)){
				y += gravityDirection;
				x -= 1;
			}else if(CanGoAtRelative(1, 0) && CanGoAtRelative(1, gravityDirection)){
				y += gravityDirection;
				x += 1;
			}
		}
	}
}
public class DirtTile : PhysicsTile
{
	private static int grassGrowTicks = 60;
	private int currentGrassTicks = 0;
	public int grassStage {get; private set;}

	DirtTileVisual dirtTileVisual;


	public DirtTile(int x, int y) : base (x,y)
	{
		this.gravityDirection = -1;
		this.visual = dirtTileVisual = new DirtTileVisual();
		dirtTileVisual.SetGrassStage(0);
	}

	public override void OnTick()
	{
		var _tileAbove = GameMaster.Instance().tileGrid.GetTileAt(x, y + 1); 
		if(_tileAbove == null || _tileAbove is CreatureTile){
			if(currentGrassTicks < grassGrowTicks){
				currentGrassTicks ++;
			}else{
				currentGrassTicks = 0;

				if(HasAccessToWater()){
					GrowGrass();
				}else{
					ReduceGrass();
				}
			}
		}else{
			grassStage = 0;
		}

		base.OnTick();
	}

	private bool HasAccessToWater ()
	{
		var _tileGrid = GameMaster.Instance().tileGrid;
		
		return _tileGrid.GetTileAt(x + 1, y) is WaterTile
		|| _tileGrid.GetTileAt(x - 1, y) is WaterTile
		|| _tileGrid.GetTileAt(x, y -1) is WaterTile

		|| _tileGrid.GetTileAt(x - 2, y) is WaterTile
		|| _tileGrid.GetTileAt(x + 2, y) is WaterTile
		|| _tileGrid.GetTileAt(x, y - 2) is WaterTile;
	}

	protected override void UpdateVisuals ()
	{
		dirtTileVisual.SetGrassStage(grassStage);
		base.UpdateVisuals();
	}

	public void SetGrassStage (int grassStage)
	{
		this.grassStage = grassStage;
		dirtTileVisual.SetGrassStage(grassStage);
	}

	public void GrowGrass ()
	{
		if(grassStage < 3){
			grassStage++;
			SetGrassStage(grassStage);
			GameMaster.Instance().gameStats.grace++;
		}
	}

	public void ReduceGrass ()
	{
		if(grassStage > 0){
			grassStage--;
			SetGrassStage(grassStage);
		}
	}
}
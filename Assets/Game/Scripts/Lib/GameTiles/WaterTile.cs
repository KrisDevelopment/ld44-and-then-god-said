using UnityEngine;

public class WaterTile : LiquidTile
{
	public WaterTile(int x, int y) : base(x, y, new Color(0.2f, 0.5f, 1))
	{
	}

	public override void OnTick ()
	{
		var _tileGrid = GameMaster.Instance().tileGrid;
		LavaInteraction(0, gravityDirection, _tileGrid);

		base.OnTick();
	}

	private void LavaInteraction (int xDir, int yDir, TileGrid _tileGrid)
	{
		var _interactionTile = _tileGrid.GetTileAt(x + xDir, y + yDir);
		if(_interactionTile is LavaTile){
			_interactionTile.Destroy();
			new StoneTile(_interactionTile.x, _interactionTile.y);
		}
	}
}
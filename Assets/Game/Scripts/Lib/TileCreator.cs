public static class TileCreator
{
	public static BaseTile CreateTile (int x, int y, TileCreatorType type)
	{
		switch(type){
			case TileCreatorType.Dirt:{
				return new DirtTile(x, y);
			}
			case TileCreatorType.Sand:{
				return new SandTile(x, y);	
			}
			case TileCreatorType.Stone:{
				return new StoneTile(x, y);
			}
			case TileCreatorType.Water:{
				return new WaterTile(x, y);
			}
			case TileCreatorType.Lava:{
				return new LavaTile(x, y);
			}
			case TileCreatorType.Sheep:{
				return new Sheep(x, y);
			}
			case TileCreatorType.Steam:{
			
			}break;
		}

		return null;
	}
}
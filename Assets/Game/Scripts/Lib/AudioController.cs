﻿using UnityEngine;

public static class AudioController
{
	public const string PREFS_KEY = "LD45VolStr";
	private static float volume = .8f;

	public static void Init ()
	{
		AudioListener.volume = GetVolume();
	}

	public static float GetVolume ()
	{
		if(PlayerPrefs.HasKey(PREFS_KEY)){
			volume = PlayerPrefs.GetFloat(PREFS_KEY);
		}

		return volume;
	}

	public static void SetVolume (float vol)
	{
		AudioListener.volume = vol;
		volume = vol;
		PlayerPrefs.SetFloat(PREFS_KEY, volume);
	}
}

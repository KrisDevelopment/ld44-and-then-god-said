using UnityEngine;

[System.Serializable]
public class GameViewElement
{
	public GameView gameView;
	public GameObject objectBinding;
}
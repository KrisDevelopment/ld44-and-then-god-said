using UnityEngine;

public abstract class LiquidTile : PhysicsTile
{
	protected LiquidTileVisual liquidTileVisual;
	protected int maxTravelDistance = 1;

	public LiquidTile(int x, int y, Color color) : base(x, y)
	{
		this.gravityDirection = -1;
		visual = (liquidTileVisual = new LiquidTileVisual(color));
		maxTravelDistance = GameMaster.Instance().tileGrid.width - 1;
	}

	protected override void MovementBehavior()
	{
		LiquidMovement();
	}

	protected override void UpdateVisuals ()
	{
		// surface waves or solid block
		var _surface = CanGoAtRelative(0, 1) && !CanGoAtRelative(0, -1);
		liquidTileVisual.SetSurface(_surface);
		base.UpdateVisuals();
	}

	protected override bool CanGoAtRelative (int xDir, int yDir)
	{
		var _tileGrid = GameMaster.Instance().tileGrid;
		var _collidingTile = _tileGrid.GetTileAt(x + xDir, y + yDir);
		return _tileGrid.IsValidTilePosition(x + xDir, y + yDir) && (_collidingTile == null);
	}

	protected void LiquidMovement ()
	{
		if(CanGoAtRelative(0, gravityDirection)) {
			y += gravityDirection;
		}else if(!CanGoAtRelative(0, -gravityDirection)){
			if(CanGoAtRelative(1, 0)){
				x += 1;
			}else if (CanGoAtRelative(-1, 0)) {
				x -= 1;
			}
		}else{
			int _foundLeftHole = -1;
			for(int i = 0; i < maxTravelDistance; i++){
				if(CanGoAtRelative(-i, gravityDirection)){
					_foundLeftHole = i;
					break;
				}
			}

			int _foundRightHole = -1;
			for(int i = 0; i < maxTravelDistance; i++){
				if(CanGoAtRelative(i, gravityDirection)){
					_foundRightHole = i;
					break;
				}
			}

			if(_foundLeftHole > -1 && _foundRightHole > -1){
				if(_foundLeftHole < _foundRightHole){
					if(CanGoAtRelative(-1, 0)){
						x -= 1;
					}
				}else if (CanGoAtRelative(1, 0)){
					x += 1;
				}
			}
			else if(_foundLeftHole > -1){
				if(CanGoAtRelative(-1, 0)){
					x -= 1;
				}
			} else if(_foundRightHole > -1){
				if(CanGoAtRelative(1, 0)){
					x += 1;
				}
			}
		}
	}
}
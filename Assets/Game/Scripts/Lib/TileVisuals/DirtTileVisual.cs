using UnityEngine;

public class DirtTileVisual : TileVisual
{
	protected TileVisual grassVisual;

	public DirtTileVisual () : base (new Color (.5f, .25f, 0), AssetRepo.Instance().earthTileSprite)
	{
		grassVisual = new TileVisual(new Color(.2f, .8f, .2f), AssetRepo.Instance().grassStage0);
	}

	public override void SetPosition3f (Vector3 value)
	{
		base.SetPosition3f(value);
		grassVisual.SetPosition3f(value + Vector3.up);
	}

	public void SetGrassStage (int stage)
	{
		Sprite _sprite;
		switch(stage){
			case 0:
				_sprite = null;
				break;
			case 1:
				_sprite = AssetRepo.Instance().grassStage0;
				break;
			case 2:
				_sprite = AssetRepo.Instance().grassStage1;
				break;
			case 3:
				_sprite = AssetRepo.Instance().grassStage2;
				break;
			default:
				throw new System.ArgumentOutOfRangeException(string.Format("{0} is not a valid stage", stage));
		}

		grassVisual.spriteRenderer.sprite = _sprite;
	}

	public override void Destroy()
	{
		base.Destroy();
		grassVisual.Destroy();
	}
}
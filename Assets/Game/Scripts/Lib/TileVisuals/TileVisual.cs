using UnityEngine;

public class TileVisual
{
	public SpriteRenderer spriteRenderer;

	public TileVisual(Color color, Sprite sprite = null)
	{
		spriteRenderer = new GameObject("Tile").AddComponent<SpriteRenderer>();
		spriteRenderer.color = color;
		spriteRenderer.transform.position = -Vector3.up * 100;

		if(sprite != null) {
			spriteRenderer.sprite = sprite;
		}
	}
	
	public virtual Vector3 GetPosition3f ()
	{
		return spriteRenderer.transform.position;
	}

	public virtual void SetPosition3f (Vector3 value)
	{
		spriteRenderer.transform.position = value;
	}

	public virtual void Destroy ()
	{
		GameObject.Destroy(spriteRenderer.gameObject);
	}
}
using System.Collections.Generic;
using UnityEngine;

public class LiquidTileVisual : TileVisual
{
	private List<Sprite> surfaceSprites;
	private List<Sprite> solidSprites;
	private SpriteAnimator spriteAnimator;

	public LiquidTileVisual(Color color) : base(color, AssetRepo.Instance().solidLiquidSprites[0])
	{
		spriteAnimator = spriteRenderer.gameObject.AddComponent<SpriteAnimator>();
		surfaceSprites = AssetRepo.Instance().surfaceLiquidSprites;
		solidSprites = AssetRepo.Instance().solidLiquidSprites;
		spriteAnimator.SetSprites(solidSprites);
	}

	public void SetSurface (bool surface)
	{
		spriteAnimator.SetSprites(surface ? surfaceSprites : solidSprites);
	}
}
using System.Collections.Generic;
using UnityEngine;

public class SheepVisual : TileVisual
{
	private SpriteAnimator spriteAnimator;
	private float horizontalOffset = 0;

	public SheepVisual() : base(Color.white, AssetRepo.Instance().sheepIdleSprites[0])
	{
		spriteAnimator = spriteRenderer.gameObject.AddComponent<SpriteAnimator>();
		spriteAnimator.SetSprites(AssetRepo.Instance().sheepIdleSprites);
	}

	public void SetSheepState (Sheep.State state)
	{
		switch (state){
			case Sheep.State.Idle:
				spriteAnimator.SetSprites(AssetRepo.Instance().sheepIdleSprites);
			break;
			case Sheep.State.Eating:
				spriteAnimator.SetSprites(new List<Sprite>{AssetRepo.Instance().sheepEatingSprite});
			break;
		}
	}

	public void SetSheepHungerVisual (float hungerinterval)
	{
		spriteRenderer.color = Color.Lerp(new Color(.5f, .5f, .5f), Color.white, hungerinterval);
	}

	public void SetLookDirection (int lookDir /* 1 = right; -1 = left */)
	{
		// default sprite state is looking to the right so if lookDir is -1 then mirror the sprite
		spriteRenderer.flipX = lookDir < 0;
		horizontalOffset = lookDir < 0 ? 1f : 0;
	}

	public override void SetPosition3f(Vector3 value)
	{
		spriteRenderer.transform.position = value + Vector3.right * horizontalOffset;
	}

	public override Vector3 GetPosition3f()
	{
		return spriteRenderer.transform.position - Vector3.right * horizontalOffset;
	}
}
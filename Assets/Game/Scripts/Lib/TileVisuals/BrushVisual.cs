using UnityEngine;

public class BrushVisual : TileVisual
{
	public enum State
	{
		Normal,
		CantPlace,
	}

	public BrushVisual() : base(Color.white, AssetRepo.Instance().brushVisualSprite)
	{
		spriteRenderer.sortingOrder = 10;
	}

	public void SetState (State state)
	{
		switch (state){
			case State.CantPlace:
				spriteRenderer.color = Color.red;
			break;
			
			default:
				spriteRenderer.color = Color.white;
			break;
		}
	}
}
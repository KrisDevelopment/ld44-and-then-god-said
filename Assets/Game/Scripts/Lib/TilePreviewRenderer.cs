using UnityEngine;

public class TilePreview
{
	public Texture2D texture;
	public readonly bool failed;
	private const int SIZE = 256;

	public TilePreview (TileCreatorType type, int yHeight)
	{
		var _tile = TileCreator.CreateTile(-10, yHeight, type);
		
		if(_tile != null && _tile.visual != null){
			_tile.visual.SetPosition3f(new Vector3(-10, yHeight, 0));

			Camera _renderCamera = new GameObject("_RenderCam_").AddComponent<Camera>();
			_renderCamera.orthographic = true;
			_renderCamera.orthographicSize = 0.5f;
			_renderCamera.transform.position = _tile.visual.GetPosition3f() - Vector3.forward + Vector3.right * 0.5f + Vector3.up * 0.5f;
			var _renderTexture = RenderTexture.GetTemporary(SIZE, SIZE);
			_renderCamera.targetTexture = _renderTexture;
			_renderCamera.enabled = false;
			
			var _initialRenderTexture = RenderTexture.active;
			RenderTexture.active = _renderTexture;
			_renderCamera.Render();
			texture = new Texture2D(SIZE, SIZE);
			texture.wrapMode = TextureWrapMode.Clamp;
			texture.ReadPixels(new Rect(0,0, SIZE, SIZE), 0, 0);
			texture.Apply();
			RenderTexture.active = _initialRenderTexture;
			
			GameObject.Destroy(_renderCamera.gameObject);
			_tile.Destroy();
			failed = false;
		}else{
			failed = true;
		}
	}

	public void Destroy ()
	{
		UnityEngine.Object.Destroy(texture);
	}
}
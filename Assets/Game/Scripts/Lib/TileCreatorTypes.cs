public enum TileCreatorType
{
	Nothing,
	Water,
	Lava,
	Dirt,
	Sand,
	Stone,
	Steam,
	Sheep,
}
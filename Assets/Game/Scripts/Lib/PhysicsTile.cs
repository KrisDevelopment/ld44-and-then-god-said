﻿public abstract class PhysicsTile : BaseTile
{
	protected int gravityDirection = 0;


	public PhysicsTile(int x, int y) : base(x, y) {}

	public override void OnTick()
	{
		var _initialX = x;
		var _initialY = y;

		MovementBehavior();
		base.OnTick();
		GameMaster.Instance().tileGrid.RegisterTileMovement(this, _initialX, _initialY);
	}

	protected virtual void MovementBehavior () {
		VerticalGravity();
	}

	// --

	protected virtual bool CanGoAtRelative (int xDir, int yDir)
	{
		var _tileGrid = GameMaster.Instance().tileGrid;
		var _collidingTile = _tileGrid.GetTileAt(x + xDir, y + yDir);
		return _tileGrid.IsValidTilePosition(x + xDir, y + yDir)
			&& (_collidingTile == null || _collidingTile is LiquidTile);
	}

	protected void VerticalGravity ()
	{		
		if(CanGoAtRelative(0, gravityDirection)){
			y += gravityDirection;
		}
	}
}
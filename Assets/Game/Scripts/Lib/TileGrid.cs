using System;
using System.Collections.Generic;
using System.Linq;
using SETUtil.Common.Extend;
using UnityEngine;

public class TileGrid
{
	public readonly BaseTile[,] tiles;
	public readonly int width;
	public readonly int height;

	
	public TileGrid (int width, int height)
	{
		tiles = new BaseTile[width, height];
		this.width = width;
		this.height = height;
	}

	public void Tick ()
	{
		ForEachTile(a => a.Tick());
		ForEachTile(a => a.ClearTickFlag());
	}

	public BaseTile GetTileAt (int x, int y)
	{
		if(!IsValidTilePosition(x,y)) {
			return null;
		}
		
		return tiles[x,y];
	}

	public bool IsValidTilePosition (int x, int y)
	{
		if(x < 0 || x >= width){
			return false;
		}
		
		if(y < 0 || y >= height){
			return false;
		}

		return true;
	}

	public void RegisterTile (BaseTile tile)
	{
		if(!IsValidTilePosition(tile.x, tile.y)){
			return;
		}

		if(tiles[tile.x, tile.y] != null){
			tiles[tile.x, tile.y].Destroy();
		}

		tiles[tile.x, tile.y] = tile;
	}

	public void RegisterTileMovement (BaseTile tile, int oldX, int oldY)
	{
		if(!IsValidTilePosition(oldX, oldY) || !IsValidTilePosition(tile.x, tile.y)){
			tile.Destroy();
			return;
		}

		var _tileAtTargetPosition = tiles[tile.x, tile.y];
		// move the tile from the new position to the old position
		tiles[oldX, oldY] = _tileAtTargetPosition;
		// move the given tile to the new position
		tiles[tile.x, tile.y] = tile;
		// update the moved tile
		if(_tileAtTargetPosition != null){
			_tileAtTargetPosition.SetPos(oldX, oldY);
		}
	}

	public void RegsiterTileDestruction (int x, int y)
	{
		if(!IsValidTilePosition(x,y)){
			return;
		}

		tiles[x,y] = null;
	}

	public void DestroyTile (int x, int y)
	{
		var _tile = GetTileAt(x,y);
		if(_tile != null){
			_tile.Destroy();
		}
	}

	public void ForEachTile (Action<BaseTile> action)
	{
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				var _tile = tiles[x,y];
				if(_tile != null) 
					action.Invoke(_tile);
			}	
		}
	}
	public int ClampX (int x)
	{
		int _x_width;
		return x < 0 ? 0 : (x > (_x_width = width) ? _x_width : x); 
	}

	public int ClampY (int y)
	{
		int _y_height;
		return y < 0 ? 0 : (y > (_y_height = height) ? _y_height : y);
	}
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineScheduler : MonoBehaviour
{
	private List<IEnumerator> coroutines = new List<IEnumerator>();

	private static CoroutineScheduler instance;

	private static CoroutineScheduler Instance ()
	{
		if(instance == null){
			instance = GameObject.FindObjectOfType<CoroutineScheduler>();
		}

		return instance;
	}

	public void Schedule (IEnumerator crt)
	{
		coroutines.Add(crt);
	}

	private void Update ()
	{
		var _crtToRemove = new List<IEnumerator>();

		foreach(var crt in coroutines){
			if(!crt.MoveNext()){
				_crtToRemove.Add(crt);
			}
		}

		foreach(var crt in _crtToRemove){
			coroutines.Remove(crt);
		}
	}

	public static void NextUpdate (Action action)
	{
		CoroutineScheduler.Instance().Schedule(Instance().NextUpdateCrt(action));
	}

	private IEnumerator NextUpdateCrt (Action action)
	{
		yield return null;
		action.Invoke();
	}
}
public class CreatureTile : PhysicsTile
{
	public CreatureTile(int x, int y) : base(x, y) {}

	public virtual void Kill ()
	{
		Destroy();
		new BloodTile(x, y);
		GameMaster.Instance().gameStats.grace -= 20;
	}

	public override void OnTick()
	{
		base.OnTick();

		var _tileGrid = GameMaster.Instance().tileGrid;
		var _tileAbove = _tileGrid.GetTileAt(x, y + 1);
		var _tileBelow = _tileGrid.GetTileAt(x, y - 1);

		if(_tileAbove is LiquidTile || _tileBelow is LiquidTile){
			Kill();
		}else if(_tileBelow is CreatureTile){
			((CreatureTile)_tileBelow).Kill();
		}
	}
}
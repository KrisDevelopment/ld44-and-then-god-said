﻿using UnityEngine;

public abstract class BaseTile
{
	public TileVisual visual;
	public int x {get; protected set;}
	public int y {get; protected set;}
	public bool ticked {get; protected set;}


	public BaseTile (int x, int y)
	{
		this.x = x;
		this.y = y;
		GameMaster.Instance().tileGrid.RegisterTile(this);
		GameMaster.Instance().gameStats.grace++;
	}

	public void SetPos (int x, int y)
	{
		this.x = x;
		this.y = y;
		Tick();
	}
	
	public void Tick ()
	{
		if(!ticked){
			OnTick();
		}
	}

	public void ClearTickFlag ()
	{
		ticked = false;
	}

	public virtual void OnTick ()
	{
		UpdateVisuals();
		ticked = true;
	}

	public virtual void Destroy ()
	{
		if(visual != null){
			visual.Destroy();
		}
		
		GameMaster.Instance().tileGrid.RegsiterTileDestruction(x, y);
		GameMaster.Instance().gameStats.grace--;
	}

	protected virtual void UpdateVisuals ()
	{
		if(visual != null){
			visual.SetPosition3f(new Vector2(x, y));
		}
	}
}

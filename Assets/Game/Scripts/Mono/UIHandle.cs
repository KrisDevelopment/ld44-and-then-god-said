using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHandle : MonoBehaviour
{
	public RectTransform tileSelectionContent;
	public GameObject uiButtonPrefab;
	public Slider audioVolumeSlider;
	public Text graceLabel;

	private static UIHandle instance;
	private List<TilePreview> tilePreviews = new List<TilePreview>();

	public static UIHandle Instance ()
	{
		if(instance == null){
			instance = GameObject.FindObjectOfType<UIHandle>();
		}

		return instance;
	}

	public void BuildTileSelection ()
	{
		foreach(Transform _child in SETUtil.SceneUtil.CollectAllChildren(tileSelectionContent)){
			GameObject.Destroy(_child.gameObject);
		}

		for(int i = 0; i < Enum.GetNames(typeof(TileCreatorType)).Length; i++){
			var _tileType = (TileCreatorType)i;
			var _preview = new TilePreview(_tileType, -(10 + i));
			
			if(_preview.failed){
				continue;
			}

			tilePreviews.Add(_preview);

			var _button = GameObject.Instantiate(uiButtonPrefab).GetComponent<Button>();
			_button.transform.SetParent(tileSelectionContent, false);
			_button.transform.GetComponentInChildren<RawImage>().texture = _preview.texture;
			_button.onClick.AddListener(delegate {BrushInput.selectedTileType = _tileType;});
		}
	}

	public void UpdateFromGameStats (GameStats gameStats)
	{
		if(gameStats == null){
			return;
		}
		
		graceLabel.color = Color.Lerp(new Color(1, 0.4f, 0.4f), Color.white, SETUtil.MathUtil.Saturate(100 + gameStats.grace));
		graceLabel.text = string.Format("Grace: {0}", gameStats.grace);
	}

	public void Awake ()
	{
		audioVolumeSlider.value = AudioController.GetVolume();
		audioVolumeSlider.onValueChanged.AddListener(delegate{ AudioController.SetVolume(audioVolumeSlider.value); });
	}

	// UI Buttons
	public void StartGameButton ()
	{
		GameViewController.Instance().SetGameView(GameView.Playing);
	}

	public void QuitGameButton ()
	{
		Application.Quit();
	}
}
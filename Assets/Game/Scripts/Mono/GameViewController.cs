﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameViewController : MonoBehaviour
{
	private GameView currentGameView = GameView.MainMenu;
	private static GameViewController instance;

	public Action<GameView, GameView> onGameViewChange;

	public List<GameViewElement> elements;


	public static GameViewController Instance ()
	{
		if(instance == null){
			instance = GameObject.FindObjectOfType<GameViewController>();
		}

		return instance;
	}

	private void Start ()
	{
		SetGameView(currentGameView);
	}

	public void SetGameView (GameView gameView)
	{
		if(onGameViewChange != null){
			onGameViewChange.Invoke(currentGameView, gameView);
		}

		currentGameView = gameView;

		foreach(var element in elements){
			element.objectBinding.SetActive(element.gameView == gameView);
		}
	}

	public bool IsGameView (GameView gameView)
	{
		return currentGameView == gameView;
	}
}

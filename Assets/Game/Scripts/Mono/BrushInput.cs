﻿using SETUtil.Common.Types;
using UnityEngine;

public class BrushInput : MonoBehaviour
{
	private BrushVisual brushVisual;
	public static TileCreatorType selectedTileType = TileCreatorType.Nothing;
	private float cooldown = 0;
	private float cooldownTime = 0.01f;

	void Awake ()
	{
		brushVisual = new BrushVisual();
	}

	void Update ()
	{
		var _worldPos = GameMaster.Instance().mainCamera.ScreenToWorldPoint(Input.mousePosition);
		_worldPos.z = -0.1f;

		int _x = Mathf.FloorToInt(_worldPos.x);
		int _y = Mathf.FloorToInt(_worldPos.y);
		brushVisual.SetPosition3f(new Vector3(_x, _y, -0.1f));

		bool _canPlaceTile = CanPlaceTileAt(_x,_y);

		if(cooldown <= 0 && _canPlaceTile){
			if(Input.GetMouseButton((int) MouseButton.Left)){
				TileCreator.CreateTile(_x, _y, selectedTileType);
				cooldown = cooldownTime;
			}
			
			if(Input.GetMouseButton((int) MouseButton.Right)){
				GameMaster.Instance().tileGrid.DestroyTile(_x, _y);
				cooldown = cooldownTime;
			}
		}else{
			cooldown -= Time.deltaTime;
		}

		brushVisual.SetState(_canPlaceTile ? BrushVisual.State.Normal : BrushVisual.State.CantPlace);
	}

	private bool CanPlaceTileAt (int x, int y)
	{
		return !(GameMaster.Instance().tileGrid.GetTileAt(x, y - 1) is CreatureTile) && GameMaster.Instance().tileGrid.IsValidTilePosition(x, y);
	}

	public void SelectTileType (TileCreatorType type)
	{
		selectedTileType = type;
	}
}

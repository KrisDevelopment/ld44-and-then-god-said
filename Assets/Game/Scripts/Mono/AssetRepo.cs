using System.Collections.Generic;
using UnityEngine;

public class AssetRepo : MonoBehaviour
{
	private static AssetRepo instance;

	public Sprite
		baseTileSprite,
		earthTileSprite,
		grassStage0,
		grassStage1,
		grassStage2;

	public List<Sprite>
		surfaceLiquidSprites,
		solidLiquidSprites,
		sheepIdleSprites;
	
	public Sprite
		sheepEatingSprite,
		sheepDeadSprite,
		brushVisualSprite;

	public static AssetRepo Instance ()
	{
		if(instance == null){
			instance = GameObject.FindObjectOfType<AssetRepo>();
		}

		return instance;
	}
}
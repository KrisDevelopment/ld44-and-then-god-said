using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimator : MonoBehaviour
{
	private List<Sprite> sprites;
	private SpriteRenderer spriteRenderer;
	private int currentSprite = 0;
	private float spriteSwitchTimer = 0;
	private float spriteSwitchTime = 0.25f;

	public void SetSprites (List<Sprite> sprites)
	{
		this.sprites = sprites;
	}

	private void Awake ()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	private void Update ()
	{
		if(spriteRenderer == null || sprites == null){
			return;
		}

		if(spriteSwitchTimer <= 0){
			spriteSwitchTimer = spriteSwitchTime;
			currentSprite = (currentSprite + 1) % sprites.Count;
			spriteRenderer.sprite = sprites[currentSprite];
		}else{
			spriteSwitchTimer -= Time.deltaTime;
		}
	}
}
﻿using UnityEngine;

public class GameMaster : MonoBehaviour
{
	public Camera mainCamera { get; private set; }
	public TileGrid tileGrid { get; private set; }
	public GameStats gameStats = new GameStats();

	private GameViewController gameViewController;
	private static GameMaster instance;
	
	private float tickInterval = .05f;
	private float lastTickTime = 1f;
	private int gameSize = 24;


	public static GameMaster Instance ()
	{
		if(instance == null){
			instance = GameObject.FindObjectOfType<GameMaster>();
		}

		return instance;
	}

	private void Awake ()
	{
		AudioController.Init();
		
		gameViewController = GameViewController.Instance();
		gameViewController.onGameViewChange += GameViewChangeListener;

		tileGrid = new TileGrid(gameSize, gameSize);
		mainCamera = Camera.main;
		mainCamera.orthographicSize = gameSize/2;
		mainCamera.transform.position = new Vector3(mainCamera.orthographicSize, mainCamera.orthographicSize, -1);
	}

	private void Update ()
	{
		if(lastTickTime <= 0){
			lastTickTime = tickInterval;
			tileGrid.Tick();
			UIHandle.Instance().UpdateFromGameStats(gameStats);
		}else{
			lastTickTime -= Time.deltaTime;
		}
	}

	private void GameViewChangeListener (GameView previous, GameView next)
	{
		if(previous == GameView.MainMenu && next == GameView.Playing){
			OnGameStart();
		}

		if(previous == GameView.Playing && next == GameView.GameOver){
			OnGameOver();
		}
	}

	private void OnDestroy ()
	{
		gameViewController.onGameViewChange -= GameViewChangeListener;
	}

	public void OnGameStart ()
	{
		UIHandle.Instance().BuildTileSelection();
		gameStats = new GameStats();
	}

	public void OnGameOver ()
	{

	}
}
